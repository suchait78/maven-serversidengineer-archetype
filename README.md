# HOW TO USE THIS ARCHETYPE #
 mvn archetype:generate -DarchetypeCatalog=local -DarchetypeArtifactId=maven-serversideengineer-archetype -DarchetypeGroupId=com.serversideengineer.archetype -DarchetypeVersion=1.0-SNAPSHOT
 
# Logs and Response #
{code}
[INFO] Scanning for projects...
[INFO]
[INFO] ------------------< org.apache.maven:standalone-pom >-------------------
[INFO] Building Maven Stub Project (No POM) 1
[INFO] --------------------------------[ pom ]---------------------------------
[INFO]
[INFO] >>> maven-archetype-plugin:3.2.0:generate (default-cli) > generate-sources @ standalone-pom >>>
[INFO]
[INFO] <<< maven-archetype-plugin:3.2.0:generate (default-cli) < generate-sources @ standalone-pom <<<
[INFO]
[INFO]
[INFO] --- maven-archetype-plugin:3.2.0:generate (default-cli) @ standalone-pom ---
[INFO] Generating project in Interactive mode
[WARNING] Archetype not found in any catalog. Falling back to central repository.
[WARNING] Add a repository with id 'archetype' in your settings.xml if archetype's repository is elsewhere.
Define value for property 'groupId': com.treeside
Define value for property 'artifactId': side-tree
Define value for property 'version' 1.0-SNAPSHOT: : 1.0
Define value for property 'package' com.treeside: : com.treeside.sidetree
Define value for property 'project-name': side-tree
[INFO] Using property: property-file-name = Resource-default
Confirm properties configuration:
groupId: com.treeside
artifactId: side-tree
version: 1.0
package: com.treeside.sidetree
project-name: side-tree
property-file-name: Resource-default
 Y: : y
[INFO] ----------------------------------------------------------------------------
[INFO] Using following parameters for creating project from Archetype: maven-serversideengineer-archetype:1.0-SNAPSHOT
[INFO] ----------------------------------------------------------------------------
[INFO] Parameter: groupId, Value: com.treeside
[INFO] Parameter: artifactId, Value: side-tree
[INFO] Parameter: version, Value: 1.0
[INFO] Parameter: package, Value: com.treeside.sidetree
[INFO] Parameter: packageInPathFormat, Value: com/treeside/sidetree
[INFO] Parameter: package, Value: com.treeside.sidetree
[INFO] Parameter: version, Value: 1.0
[INFO] Parameter: groupId, Value: com.treeside
[INFO] Parameter: project-name, Value: side-tree
[INFO] Parameter: property-file-name, Value: Resource-default
[INFO] Parameter: artifactId, Value: side-tree
[WARNING] Don't override file E:\Suchait\suchait-code-practice\side-tree\src\main\java\com\treeside\sidetree
[WARNING] Don't override file E:\Suchait\suchait-code-practice\side-tree\src\test\java\com\treeside\sidetree
[INFO] Project created from Archetype in dir: E:\Suchait\suchait-code-practice\side-tree
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  01:27 min
[INFO] Finished at: 2020-11-04T20:37:03+05:30
[INFO] ------------------------------------------------------------------------
{code}