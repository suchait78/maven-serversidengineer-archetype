package ${package}.config;

import java.net.URI;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import  ${package}.consul.ConsulDiscoveryClient;

@Configuration
public class ConsulConfig {

	@Autowired
	private DiscoveryClient discoveryClient;
	 
	
}
