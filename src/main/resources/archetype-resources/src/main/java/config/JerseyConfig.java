package ${package}.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import  ${package}.filter.RequestContextFilter;

@ApplicationPath("/${project-name}")
@Configuration
public class JerseyConfig extends ResourceConfig{

	public JerseyConfig() {
		register(RequestContextFilter.class);
	}
}
