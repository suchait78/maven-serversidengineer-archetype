package ${package}.config;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePropertySource;

@Configuration
public class ExternalPropertySource {

	@Value("${external.config.location}")
	private String externalConfigLocation;
	
	@Autowired
	private ConfigurableEnvironment env;
	
	@Bean
	public ResourcePropertySource getProperties() throws IOException {

		Resource resource = new FileSystemResource(externalConfigLocation);
		ResourcePropertySource resourcePropertySource = new ResourcePropertySource(resource);

		MutablePropertySources sources = env.getPropertySources();
		sources.addFirst(resourcePropertySource);

		return resourcePropertySource;
	}
}
