package ${package}.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HealthConfig {

	/*
	 * Below is the sample code to include other component's health check
	 * Create single bean for every component.
	 * 
	@Value("${component.a.health.url}")
	private String componentAHealthUrl;

	@Bean
	public ComponentHealthCheck componentAHealthCheck() {
		return new ComponentHealthCheck(componentAHealthUrl);
	}
	*/
		
}
