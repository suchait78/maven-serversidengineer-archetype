package ${package}.health;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import  ${package}.constant.Constants;
import  ${package}.jersey.JerseyClientConfig;

public class ComponentHealthCheck implements HealthIndicator{

	public static final Logger LOG = LoggerFactory.getLogger(ComponentHealthCheck.class);
	
	@Autowired
	private JerseyClientConfig jerseyConfig;
	
	private String componentHealthUrl;
	
	public ComponentHealthCheck() {
		// TODO Auto-generated constructor stub
	}
	
	public ComponentHealthCheck(String componentHealthUrl) {
		this.componentHealthUrl = componentHealthUrl;
	}
	
	@Override
	public Health health() {
		
		LOG.info("Health Check URL : {}", this.componentHealthUrl);
		
		Health.Builder status = Health.up();
		
		try {
			Response componentHealthResponse = getHealthResponse(this.componentHealthUrl);
			
			if(componentHealthResponse.getStatus() != Response.Status.OK.getStatusCode()) {		
				status = Health.down();
			}
		} catch (Exception e) {	
			LOG.error("Exception : {} occurred in health check of URL : {} ", e.getMessage(), this.componentHealthUrl);
			
			status = Health.down();
		}
			
		return status.build();
	}

	private Response getHealthResponse(String componentHealthUrl) {
		
		Client client = ClientBuilder.newClient(jerseyConfig.getClientConfig());
		
		WebTarget webTarget = client.target(componentHealthUrl);
		
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header(Constants.CONTENT_TYPE, MediaType.APPLICATION_JSON);
		
		return invocationBuilder.get();
	}
	
	
}
