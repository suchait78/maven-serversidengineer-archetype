package ${package}.consul;

import java.net.URI;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.discovery.DiscoveryClient;

public class ConsulDiscoveryClient {

	public static final Logger LOG = LoggerFactory.getLogger(ConsulDiscoveryClient.class);
	
	private DiscoveryClient discoveryClient;
	
	private String applicationName;
	
	public ConsulDiscoveryClient() {
		
	}
	
	public ConsulDiscoveryClient(String applicationName){
		this.applicationName = applicationName;
	}
	
	public DiscoveryClient getDiscoveryClient() {
		return discoveryClient;
	}

	public void setDiscoveryClient(DiscoveryClient discoveryClient) {
		this.discoveryClient = discoveryClient;
	}
	
	public Optional<URI> getServiceURL(){
		
		LOG.info("Looking up for service :" + this.applicationName);
		
		return discoveryClient.getInstances(applicationName).stream().findFirst().map(s -> s.getUri());
	}
	
}
