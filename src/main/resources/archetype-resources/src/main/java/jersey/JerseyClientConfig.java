package ${package}.jersey;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.condition.RequestCondition;

@Component
public class JerseyClientConfig {

	@Value("${jersey.read.timeout}")
	private String readTimeout;

	@Value("${jersey.connect.timeout}")
	private String connectTimeout;

	public ClientConfig getClientConfig() {

		ClientConfig clientConfig = new ClientConfig();
		clientConfig.property(ClientProperties.READ_TIMEOUT, readTimeout);
		clientConfig.property(ClientProperties.CONNECT_TIMEOUT, connectTimeout);
		
		clientConfig.register(RequestCondition.class);
		
		return clientConfig;
	}
}
