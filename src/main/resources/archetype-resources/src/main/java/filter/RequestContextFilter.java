package ${package}.filter;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class RequestContextFilter implements ContainerRequestFilter, ContainerResponseFilter{

	public static final Logger LOG = LoggerFactory.getLogger(RequestContextFilter.class);
	
	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		
		LOG.info("INCOMING REQUEST URL << {} {} ", requestContext.getMethod(), requestContext.getUriInfo().getPath());
		LOG.info("INCOMING REQUEST HEADER << {}" , requestContext.getHeaders());
		
		LOG.info("INCOMING REQUEST BODY << {}", requestContext.getEntityStream());
		
	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		
		LOG.info("OUTGOING REQUEST URL >> {} {} ", requestContext.getMethod() , requestContext.getUriInfo().getPath());
		LOG.info("OUTGOING REQUEST HEADER >> {}" , requestContext.getHeaders());
	}

}
